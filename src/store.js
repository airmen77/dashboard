import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    enterprise: [
      {
        name: 'develop',
        departament: [
          {
            id: 1,
            name: 'vasa pupkin',
            tel: '5555555',
            mail: 'vasa@sds.ds',
            skype: 'asdsad',
            telegram: "fsdfdsf",
            coment: "test test"
          },
          {
            id: 2,
            name: 'luda schekotun',
            tel: '7777777',
            mail: 'luda@sds.ds',
            skype: 'ffdfd',
            telegram: "fdsf",
            coment: "luda coment"
          }
        ]
      },
      {
        name: 'qa',
        departament: [
          {
            id: 11,
            name: 'asd dsa',
            tel: '4444',
            mail: 'asd@sds.ds',
            skype: 'asdsad',
            telegram: "fsdfdsf",
            coment: "testdd test"
          }
        ]
      },
      {
        name: 'marketing',
        departament: [
          {
            id: 21,
            name: 'vasa pupkindsa',
            tel: '745454',
            mail: 'dsad@sds.ds',
            skype: 'asdasdsad',
            telegram: "xcvxcv",
            coment: "tessdft tesfsdt"
          },
          {
            id: 22,
            name: 'luddfda fdfdf',
            tel: '21545545',
            mail: 'ludddda@sds.ds',
            skype: 'fffdsfdfd',
            telegram: "fdfsdfsf",
            coment: "luddsfa coment"
          }
        ]
      },
      {
        name: 'hr',
        departament: [
            {
              id: 31,
              name: 'hr 1',
              tel: 'dfg',
              mail: 'vasa@sds.ds',
              skype: 'asdsad',
              telegram: "fsdfdsf",
              coment: "test test"
            },
            {
              id: 32,
              name: 'hr 2',
              tel: 'dfgfdgfdg',
              mail: 'luda@sds.ds',
              skype: 'ffdfd',
              telegram: "fdsf",
              coment: "luda coment"
            }
          ]
      },
      {
        name: 'accountant',
        departament: [
            {
              id: 41,
              name: 'accountant 1',
              tel: 'dfg',
              mail: 'vasa@sds.ds',
              skype: 'asdsad',
              telegram: "fsdfdsf",
              coment: "test test"
            },
            {
              id: 42,
              name: 'accountant 2',
              tel: 'dfgfdgfdg',
              mail: 'luda@sds.ds',
              skype: 'ffdfd',
              telegram: "fdsf",
              coment: "luda coment"
            }
          ]
      },
    ]
  },
  mutations: {
    addUser(state, user){
      let newUser = user[0];
      let key = user[1];
      state.enterprise[key].departament.push(newUser);
    },
    delUser(state, key){
     state.enterprise[key[0]].departament.splice(key[1], 1)
   },
   saveUser(state, user){
     let curentUser = user[0];
     let key = user[1];
     state.enterprise[key].departament.join(curentUser)
   }
  },
  actions: {
    addUser(context, user){
      let newUser = user[0];
      let key = user[1];
      context.commit('addUser', [newUser, key])
    },
    delUser(context, key){
      context.commit('delUser', [key[0], key[1]]);
    },
    saveUser(context, user){
      let curentUser = user[0];
      let key = user[1];
      context.commit('saveUser', [curentUser, key]);
    }
  },
  getters: {
    enterprise: state => {
      return state.enterprise
    }
  }
})
